<?php

namespace Drupal\display_fields;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a display fields config entity type.
 */
interface DisplayFieldsConfigInterface extends ConfigEntityInterface {

}
