<?php

namespace Drupal\display_fields\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\display_fields\DisplayFieldsConfigInterface;

/**
 * Defines the display fields config entity type.
 *
 * @ConfigEntityType(
 *   id = "display_fields_config",
 *   label = @Translation("Display Fields Config"),
 *   label_collection = @Translation("Display Fields Configs"),
 *   label_singular = @Translation("display fields config"),
 *   label_plural = @Translation("display fields configs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count display fields config",
 *     plural = "@count display fields configs",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\display_fields\DisplayFieldsConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\display_fields\Form\DisplayFieldsConfigForm",
 *       "edit" = "Drupal\display_fields\Form\DisplayFieldsConfigForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "display_fields_config",
 *   admin_permission = "administer display_fields_config",
 *   links = {
 *     "collection" = "/admin/structure/display-fields-config",
 *     "add-form" = "/admin/structure/display-fields-config/add",
 *     "edit-form" = "/admin/structure/display-fields-config/{display_fields_config}",
 *     "delete-form" = "/admin/structure/display-fields-config/{display_fields_config}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "targetEntityType",
 *     "bundle",
 *     "display_fields"
 *   }
 * )
 */
class DisplayFieldsConfig extends ConfigEntityBase implements DisplayFieldsConfigInterface {

  /**
   * The display fields config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The display fields config label.
   *
   * @var string
   */
  protected $label;

  /**
   * The display fields config status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The display_fields_config description.
   *
   * @var string
   */
  protected $description;

}
