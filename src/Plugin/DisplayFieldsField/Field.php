<?php

namespace Drupal\display_fields\Plugin\DisplayFieldsField;

use Drupal\Core\Form\FormStateInterface;
use Drupal\display_fields\DisplayFields;

/**
 * The base plugin to create display fields field.
 */
abstract class Field extends DisplayFieldsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldFormRow($field_name, $field, $field_display_settings, $view_mode, FormStateInterface $form_state, &$complete_form) {
    $regions = array_keys($this->getFieldFormRowRegions());

    $field_row = [
      '#attributes' => ['class' => ['draggable', 'tabledrag-leaf']],
      '#row_type' => 'extra_field',
      '#region_callback' => [$this, 'getFieldFormRowRegion'],
      '#js_settings' => ['rowHandler' => 'field'],
      'human_name' => ['#markup' => $field['label']],
      'weight' => [
        '#type' => 'textfield',
        '#title' => t('Weight for @title', ['@title' => $field['label']]),
        '#title_display' => 'invisible',
        '#default_value' => $field_display_settings ? $field_display_settings['weight'] : '0',
        '#size' => 3,
        '#attributes' => ['class' => ['field-weight']],
      ],
      'parent_wrapper' => [
        'parent' => [
          '#type' => 'select',
          '#title' => t('Label display for @title', ['@title' => $field['label']]),
          '#title_display' => 'invisible',
          '#options' => array_combine($regions, $regions),
          '#empty_value' => '',
          '#attributes' => ['class' => ['field-parent']],
          '#parents' => ['fields', "display_fields_$field_name", 'parent'],
        ],
        'hidden_name' => [
          '#type' => 'hidden',
          '#default_value' => "display_fields_$field_name",
          '#attributes' => ['class' => ['field-name']],
        ],
      ],
      'region' => [
        '#type' => 'select',
        '#title' => t('Region for @title', ['@title' => $field['label']]),
        '#title_display' => 'invisible',
        '#options' => $this->getRegionOptions(),
        '#default_value' => $field_display_settings['region'] ? $field_display_settings['region'] : 'hidden',
        '#attributes' => ['class' => ['field-region']],
      ],
      'label' => [
        '#type' => 'select',
        '#title' => t('Label display for @title', ['@title' => $field['label']]),
        '#title_display' => 'invisible',
        '#options' => $this->getFieldFormFieldLabelOptions(),
        '#default_value' => $field_display_settings['label'] ?? 'above',
      ],
      'plugin' => [
        'type' => [
          '#type' => 'select',
          '#title' => t('Visibility for @title', ['@title' => $field['label']]),
          '#title_display' => 'invisible',
          '#options' => $this->getExtraFieldVisibilityOptions(),
          '#default_value' => $field_display_settings ? 'visible' : 'hidden',
          '#parents' => ['fields', "display_fields_$field_name", 'type'],
          '#attributes' => ['class' => ['field-plugin-type']],
        ],
      ],
      'settings_summary' => [],
      'settings_edit' => [],
      'display_fields_delete' => [],
    ];

    $field_row['display_fields_delete'] = [
      '#type' => 'image_button',
      '#src' => $this->pathResolver->getPath('module', 'display_fields') . '/misc/delete.png',
      '#name' => "display_fields_{$field_name}_delete",
      '#op' => 'delete',
      '#attributes' => ['alt' => t('Delete'), 'title' => t('Delete')],
      // Do not check errors for the 'Edit' button, but make sure we get
      // the value of the 'plugin type' select.
      '#limit_validation_errors' => ['fields', ["display_fields_$field_name", 'type']],
      '#validate' => [[$this, 'deleteDisplayFieldSubmit']],
      '#field_name' => $field_name,
    ];

    return $field_row;
  }

  /**
   * Form submission handler for deleting a display field.
   */
  public function deleteDisplayFieldSubmit($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $op = $trigger['#op'];
    if ($op == 'delete') {
      // Store the field whose settings are currently being edited.
      $field_name = $trigger['#field_name'];
      DisplayFields::deleteDisplayFields($this->getEntityTypeId(), $this->bundle(), $field_name);
    }
  }

  /**
   * Retrieves the region to which a row in the display overview belongs.
   *
   * @param array $row
   *   The row element.
   *
   * @return string|null
   *   The region name to which this row belongs, or NULL.
   */
  public function getFieldFormRowRegion(&$row) {
    $regions = $this->getRegions();
    if (!isset($regions[$row['region']['#value']])) {
      $row['region']['#value'] = 'hidden';
    }
    return $row['region']['#value'];
  }

  /**
   * Retrieves the default regions from the entity display overview settings.
   *
   * @return array
   *   The default regions.
   */
  public function getFieldFormRowRegions() {
    return [
      'content' => [
        'title' => t('Content'),
        'invisible' => TRUE,
        'message' => t('No field is displayed.'),
      ],
      'hidden' => [
        'title' => t('Disabled'),
        'message' => t('No field is hidden.'),
      ],
    ];
  }

  /**
   * Retrieves the visibility options for the field labels.
   *
   * @return array
   *   The visibility options.
   */
  public function getFieldFormFieldLabelOptions() {
    return [
      'above' => t('Above'),
      'inline' => t('Inline'),
      'hidden' => '- ' . t('Hidden') . ' -',
      'visually_hidden' => '- ' . t('Visually Hidden') . ' -',
    ];
  }

  /**
   * Retrieves the visibility options for the extra fields.
   *
   * @return array
   *   The visibility options.
   */
  public function getExtraFieldVisibilityOptions() {
    return [
      'visible' => t('Visible'),
      'hidden' => '- ' . t('Hidden') . ' -',
    ];
  }

  /**
   * Gets the regions necessary to create the overview form.
   *
   * @return array
   *   Example usage:
   *   @code
   *     return array(
   *       'content' => array(
   *         // label for the region.
   *         'title' => $this->t('Content'),
   *         // Indicates if the region is visible in the UI.
   *         'invisible' => TRUE,
   *         // A message to indicate that there is nothing to be displayed in
   *         // the region.
   *         'message' => $this->t('No field is displayed.'),
   *       ),
   *     );
   *   @endcode
   */
  public function getRegions() {
    return [
      'content' => [
        'title' => t('Content'),
        'invisible' => TRUE,
        'message' => t('No field is displayed.'),
      ],
      'hidden' => [
        'title' => t('Disabled', [], ['context' => 'Plural']),
        'message' => t('No field is hidden.'),
      ],
    ];
  }

  /**
   * Retrieves the region options.
   *
   * @return array
   *   The region options.
   */
  public function getRegionOptions() {
    $options = [];
    foreach ($this->getRegions() as $region => $data) {
      $options[$region] = $data['title'];
    }
    return $options;
  }

}
