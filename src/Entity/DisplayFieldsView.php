<?php

namespace Drupal\display_fields\Entity;

use Drupal\Core\Entity\EntityDisplayBase;
use Drupal\Core\Entity\EntityDisplayPluginCollection;

/**
 * Defines the display_fields view entity type.
 *
 * @ConfigEntityType(
 *   id = "display_fields_view",
 *   label = @Translation("Display fields View"),
 *   label_collection = @Translation("Display fields Views"),
 *   label_singular = @Translation("display_fields view"),
 *   label_plural = @Translation("display_fields views"),
 *   label_count = @PluralTranslation(
 *     singular = "@count display_fields view",
 *     plural = "@count display_fields views",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\display_fields\DisplayFieldsViewListBuilder",
 *     "form" = {
 *       "add" = "Drupal\display_fields\Form\DisplayFieldsViewForm",
 *       "edit" = "Drupal\display_fields\Form\DisplayFieldsViewForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "display_fields_view",
 *   admin_permission = "administer display_fields_view",
 *   links = {
 *     "edit-form" = "/admin/structure/display-fields-view/{display_fields_view}",
 *     "delete-form" = "/admin/structure/display-fields-view/{display_fields_view}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "targetEntityType",
 *     "bundle",
 *     "mode",
 *     "content",
 *     "hidden"
 *   }
 * )
 */
class DisplayFieldsView extends EntityDisplayBase {

  /**
   * The display_fields view ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The display_fields view label.
   *
   * @var string
   */
  protected $label;

  /**
   * The display_fields view status.
   *
   * @var bool
   */
  protected $status;

  /**
   * {@inheritdoc}
   */
  protected $displayContext = 'view';

  /**
   * Field formatter plugin manager.
   *
   * @var mixed
   */
  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    $this->pluginManager = \Drupal::service('plugin.manager.field.formatter');

    parent::__construct($values, $entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getRenderer($field_name) {
    if (isset($this->plugins[$field_name])) {
      return $this->plugins[$field_name];
    }

    // Instantiate the formatter object from the stored display properties.
    if (($configuration = $this->getComponent($field_name)) &&
      isset($configuration['type']) &&
      ($definition = $this->getFieldDefinition($field_name))) {
      $formatter = $this->pluginManager->getInstance([
        'field_definition' => $definition,
        'view_mode' => $this->originalMode,
        // No need to prepare, defaults have been merged in setComponent().
        'prepare' => FALSE,
        'configuration' => $configuration,
      ]);
    }
    else {
      $formatter = NULL;
    }

    // Persist the formatter object.
    $this->plugins[$field_name] = $formatter;
    return $formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    $configurations = [];
    foreach ($this->getComponents() as $field_name => $configuration) {
      if (!empty($configuration['type']) && ($field_definition = $this->getFieldDefinition($field_name))) {
        $configurations[$configuration['type']] = $configuration + [
          'field_definition' => $field_definition,
          'view_mode' => $this->originalMode,
        ];
      }
    }

    return ['formatters' => new EntityDisplayPluginCollection($this->pluginManager, $configurations)];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    // Dependencies should be recalculated on every save. This ensures stale
    // dependencies are never saved.
    if (isset($this->dependencies['enforced'])) {
      $dependencies = $this->dependencies['enforced'];
      $this->dependencies = $dependencies;
      $this->dependencies['enforced'] = $dependencies;
    }
    else {
      $this->dependencies = [];
    }

    // Configuration entities need to depend on the providers of any plugins
    // that they store the configuration for.
    foreach ($this->getPluginCollections() as $plugin_collection) {
      foreach ($plugin_collection as $instance) {
        $this->calculatePluginDependencies($instance);
      }
    }

    // Configuration entities need to depend on the providers of any third
    // parties that they store the configuration for.
    foreach ($this->getThirdPartyProviders() as $provider) {
      $this->addDependency('module', $provider);
    }

    return $this->dependencies;
  }

}
