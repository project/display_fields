<?php

namespace Drupal\display_fields;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a display_fields view entity type.
 */
interface DisplayFieldsViewInterface extends ConfigEntityInterface {

}
