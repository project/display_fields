<?php

namespace Drupal\display_fields\Plugin\DisplayFieldsField;

use Drupal\Component\Plugin\PluginBase as ComponentPluginBase;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for display fields plugins.
 */
abstract class DisplayFieldsFieldBase extends ComponentPluginBase implements DisplayFieldsFieldInterface, ContainerFactoryPluginInterface {
  use DependencySerializationTrait;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $pathResolver;

  // phpcs:disable Drupal.Files.LineLength.TooLong

  /**
   * Constructs a new \Drupal\display_fields\Plugin\DisplayFieldsField\DisplayFieldsFieldBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $path_resolver
   *   The extension path resolver.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, ExtensionPathResolver $path_resolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration += $this->defaultConfiguration();
    $this->pathResolver = $path_resolver;
  }

  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('extension.path.resolver'));
  }

  /**
   * {@inheritdoc}
   */
  public function createForm($form, FormStateInterface $form_state, $parents = []) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createFormSubmit($form, FormStateInterface $form_state, $parents = []) {}

  /**
   * {@inheritdoc}
   */
  public function getFieldBuild($entities, $display_field, $display_settings, $parent_entity, $view_mode, $language) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldFormRow($field_name, $field, $field_display_settings, $view_mode, FormStateInterface $form_state, &$complete_form) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->configuration;
  }

  /**
   * Gets the current entity type.
   */
  public function getEntityTypeId() {
    return $this->configuration['entity_type'] ?? '';
  }

  /**
   * Gets the current bundle.
   */
  public function bundle() {
    return $this->configuration['bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    // By default there are no dependencies.
  }

}
