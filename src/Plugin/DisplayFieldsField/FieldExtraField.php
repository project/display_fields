<?php

namespace Drupal\display_fields\Plugin\DisplayFieldsField;

use Drupal\Core\Form\FormStateInterface;
use Drupal\display_fields\DisplayFields;

/**
 * Plugin that renders an extra field.
 *
 * @DisplayFieldsField(
 *   id = "custom_extra_field",
 *   title = @Translation("Add a custom field"),
 *   entity_types = {},
 * )
 */
class FieldExtraField extends Field {

  /**
   * {@inheritdoc}
   */
  public function createForm($form, FormStateInterface $form_state, $parents = []) {
    // The current entity for the form "Manage display".
    $entity_type = $form_state->getCompleteForm()['#entity_type'];
    $bundle = $form_state->getCompleteForm()['#bundle'];
    $view_mode = $this->getCurrentViewMode();

    // Prepare the form to create the form for adding a new custom extra field.
    $form['bundle'] = [
      '#type' => 'hidden',
      '#value' => $bundle,
    ];

    $form['view_mode'] = [
      '#type' => 'hidden',
      '#value' => $view_mode,
    ];

    $form['entity_type'] = [
      '#type' => 'hidden',
      '#value' => $entity_type,
    ];

    $form['class'] = [
      '#type' => 'textfield',
      '#default_value' => '',
      '#title' => t('Class'),
    ];

    $form['tag'] = [
      '#type' => 'select',
      '#options' => [
        'span' => 'SPAN',
        'div' => 'DIV',
        'strong' => 'STRONG',
        'h1' => 'H1',
        'h2' => 'H2',
        'h3' => 'H3',
        'h4' => 'H4',
        'h5' => 'H5',
        'h6' => 'H6',
      ],
      '#default_value' => 'div',
      '#title' => t('Tag'),
    ];

    $form['data'] = [
      '#type' => 'textarea',
      '#default_value' => '',
      '#title' => t('Write text here'),
      '#description' => t('Add html code with valid tokens if you want.'),
    ];

    // Add the token browser at the bottom.
    $form['token'] = $this->tokenBrowser();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldFormRow($field_name, $field, $field_display_settings, $view_mode, FormStateInterface $form_state, &$complete_form) {
    $row = parent::buildFieldFormRow($field_name, $field, $field_display_settings, $view_mode, $form_state, $complete_form);

    // Base button element for the various plugin settings actions.
    $base_button = [
      '#submit' => [[$this, 'multistepSubmit']],
      '#ajax' => [
        'callback' => [$this, 'multistepAjax'],
        'wrapper' => 'field-display-overview-wrapper',
        'effect' => 'fade',
      ],
      '#field_name' => "display_fields_$field_name",
    ];

    if (!empty($field_display_settings) && $form_state->get('plugin_settings_edit') === "display_fields_$field_name") {
      // We are currently editing this field's plugin settings. Display the
      // settings form and submit buttons.
      // Process values from formatters settings (these are set via ajax,
      // so check it).
      $row['plugin'] = [
        'type' => [
          '#type' => 'select',
          '#title' => t('Plugin for @title', ['@title' => $field['label']]),
          '#title_display' => 'invisible',
          '#options' => $this->getExtraFieldVisibilityOptions(),
          '#default_value' => $field_display_settings['type'] ?? 'hidden',
          '#parents' => ['fields', "display_fields_$field_name", 'type'],
          '#attributes' => ['class' => ['field-plugin-type']],
        ],
      ];

      // Generate the settings form and allow other modules to alter it.
      $row['plugin']['#cell_attributes'] = ['colspan' => 3];
      $settings_form = $this->fieldRowSettingsForm($complete_form, $form_state, $field);
      $row['plugin']['settings_edit_form'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['field-plugin-settings-edit-form']],
        '#parents' => ['fields', "display_fields_$field_name", 'settings_edit_form'],
        'label' => [
          '#markup' => t('Plugin settings'),
        ],
        'settings' => $settings_form,
        'actions' => [
          '#type' => 'actions',
          'save_settings' => $base_button + [
            '#type' => 'submit',
            '#button_type' => 'primary',
            '#name' => "display_fields_{$field_name}_plugin_settings_update",
            '#value' => t('Update'),
            '#op' => 'update',
          ],
          'cancel_settings' => $base_button + [
            '#type' => 'submit',
          '#name' => "display_fields_{$field_name}_plugin_settings_cancel",
            '#value' => t('Cancel'),
            '#op' => 'cancel',
            // Do not check errors for the 'Cancel' button, but make sure we
            // get the value of the 'plugin type' select.
            '#limit_validation_errors' => [['fields', "display_fields_$field_name", 'type']],
          ],
        ],
      ];
      $row['#attributes']['class'][] = 'field-plugin-settings-editing';
    }
    elseif (!empty($field_display_settings)) {
      // We suppose that the form is not opened.
      // Show some info about the field as summary.
      $row['settings_summary'] += $this->fieldRowSettingsSummary($field);

      // Insert the edit button.
      $row['settings_edit'] += $base_button + [
        '#type' => 'image_button',
        '#name' => "display_fields_{$field_name}_settings_edit",
        '#src' => 'core/misc/icons/787878/cog.svg',
        '#attributes' => [
          'class' => ['field-plugin-settings-edit'],
          'alt' => t('Edit'),
        ],
        '#op' => 'edit',
        // Do not check errors for the 'Edit' button, but make sure we get
        // the value of the 'plugin type' select.
        '#limit_validation_errors' => [['fields', "display_fields_$field_name", 'type']],
        '#prefix' => '<div class="field-plugin-settings-edit-wrapper">',
        '#suffix' => '</div>',
      ];
    }

    return $row;
  }

  /**
   * Return the form for editing the custom extra field.
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $field
   *
   * @return mixed
   */
  protected function fieldRowSettingsForm($form, FormStateInterface $form_state, $field) {
    // Add the token browser at the bottom.
    // Get all defaults values( label, class, tag and text).
    $field_name = $field['label'];
    $class = $field['settings']['class'];
    $tag = $field['settings']['tag'];
    $data = $field['settings']['data'];
    $elements['label'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $field_name,
    ];
    $elements['class'] = [
      '#type' => 'textfield',
      '#title' => t('Class'),
      '#default_value' => $class,
    ];
    $elements['tag'] = [
      '#type' => 'select',
      '#options' => [
        'span' => 'SPAN',
        'div' => 'DIV',
        'strong' => 'STRONG',
        'h1' => 'H1',
        'h2' => 'H2',
        'h3' => 'H3',
        'h4' => 'H4',
        'h5' => 'H5',
        'h6' => 'H6',
      ],
      '#title' => t('Tag'),
      '#default_value' => $tag,
    ];
    $elements['data'] = [
      '#type' => 'textarea',
      '#title' => t('Text'),
      '#default_value' => $data,
    ];
    // Add the token browser at the top.
    $elements['token'] = $this->tokenBrowser();
    return $elements;
  }

  /**
   * Gets the token browser.
   *
   * @return array
   *   The render array for the token browser, if the Token module is
   *   installed, or for a short description about how to use tokens.
   */
  protected function tokenBrowser() {
    $element['intro_text'] = [
      '#markup' => '<p>' . t("Configure the tokens below. Use tokens to avoid redundant meta data and search engine penalization. For example, a 'keyword' value of 'example' will be shown on all content using this configuration, whereas using the [node:field_keywords] automatically inserts the 'keywords' values from the node.") . '</p>',
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $element['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => 'all',
        '#global_types' => TRUE,
        '#click_insert' => TRUE,
        '#show_restricted' => FALSE,
        '#recursion_limit' => 3,
        '#text' => t('Browse available tokens'),
      ];
    }

    return $element;
  }

  /**
   * Builds the field row settings summary markup.
   *
   * @param array $field
   *   Current field build.
   *
   * @return array
   *   The render array for the field row settings summary.
   */
  protected function fieldRowSettingsSummary(array $field) {
    $class = t('Class: @class', ['@class' => $field['settings']['class'] ?? t('none')]);
    $tag = t('HTML wrapping tag: @tag', ['@tag' => $field['settings']['tag'] ?? t('undefined')]);

    $summary = [
      '#type' => 'markup',
      '#markup' => '<div class="field-plugin-summary">' . implode('<br />', [$class, $tag]) . '</div>',
      '#cell_attributes' => ['class' => ['field-plugin-summary-cell']],
    ];

    return $summary;
  }

  /**
   * Form submission handler for multistep buttons.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function multistepSubmit(array $form, FormStateInterface &$form_state) {
    // Get the triggered element.
    $trigger = $form_state->getTriggeringElement();
    $field_name = $trigger['#field_name'];

    // Get the entity type and the bundle associated to the custom extra field.
    $entity_type = $form['#entity_type'];
    $bundle = $form['#bundle'];

    // Get all changes in values after submission.
    $values = $form_state->getValues()['fields'][$field_name]['settings_edit_form']['settings'];

    // Get the operation (edit, update, cancel).
    $op = $trigger['#op'];
    switch ($op) {
      case 'edit':
        // Store the field whose settings are currently being edited.
        $form_state->set('plugin_settings_edit', $field_name);
        break;

      case 'update':
        if ($plugin_settings = $form_state->getValue([
          'fields',
          $field_name,
          'settings_edit_form',
          'settings',
        ])) {
          $form_state->set([
            'plugin_settings',
            $field_name,
            'settings',
          ], $plugin_settings);
        }
        // Get the current configuration of the custom extra field.
        $display_fields_config = DisplayFields::getDisplayFields($entity_type, $bundle);
        $display_fields = $display_fields_config->get('display_fields');
        $new_field_name = str_replace("display_fields_", "", $field_name);

        // Replace the old values of the custom extra field by the news one
        // after submit.
        $display_fields[$new_field_name]['label'] = $values['label'];
        $display_fields[$new_field_name]['settings']['class'] = $values['class'];
        $display_fields[$new_field_name]['settings']['tag'] = $values['tag'];
        $display_fields[$new_field_name]['settings']['data'] = $values['data'];
        $display_fields_config->set('display_fields', $display_fields);
        $display_fields_config->save();
        $form_state->set('plugin_settings_edit', NULL);
        break;

      case 'cancel':
        // Set the field back to 'non edit' mode.
        $form_state->set('plugin_settings_edit', NULL);
        break;

      case 'refresh_table':
        // If the currently edited field is one of the rows to be refreshed, set
        // it back to 'non edit' mode.
        $updated_rows = explode(' ', $form_state->getValue('refresh_rows'));
        $plugin_settings_edit = $form_state->get('plugin_settings_edit');
        if ($plugin_settings_edit && in_array($plugin_settings_edit, $updated_rows)) {
          $form_state->set('plugin_settings_edit', NULL);
        }
        break;
    }
    $form_state->setRebuild();
  }

  /**
   * Ajax handler for multistep buttons.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form for the ajax callback.
   */
  public function multistepAjax(array $form, FormStateInterface &$form_state) {
    $trigger = $form_state->getTriggeringElement();
    $op = $trigger['#op'];
    $updated_columns = [];
    $updated_rows = [];

    // Pick the elements that need to receive the ajax-new-content effect.
    switch ($op) {
      case 'edit':
        $updated_columns = ['plugin'];
        $updated_rows = [$trigger['#field_name']];
        break;

      case 'update':
      case 'cancel':
        $updated_columns = ['plugin', 'settings_summary', 'settings_edit'];
        $updated_rows = [$trigger['#field_name']];
        break;

      case 'refresh_table':
        $updated_columns = ['settings_summary', 'settings_edit'];
        $updated_rows = array_values(explode(' ', $form_state->getValue('refresh_rows')));
        break;
    }

    foreach ($updated_rows as $name) {
      foreach ($updated_columns as $key) {
        $element = &$form['fields'][$name][$key];
        $element['#prefix'] = '<div class="ajax-new-content">' . ($element['#prefix'] ?? '');
        $element['#suffix'] = ($element['#suffix'] ?? '') . '</div>';
      }
    }

    // Return the whole table.
    return $form['fields'];
  }

  /**
   * Build the display of the custom extra field.
   * @param array $entities
   * @param array $display_field
   * @param array $display_settings
   * @param string $parent_entity
   * @param string $view_mode
   * @param string $language
   *
   * @return array
   */
  public function getFieldBuild($entities, $display_field, $display_settings, $parent_entity, $view_mode, $language) {
    $build = [];
    // Get the text to display
    $data = $display_field['settings']['data'];
    // Get the class added to the text.
    $class = $display_field['settings']['class'];
    // Get the tag chosen in the configuration page.
    $tag = $display_field['settings']['tag'];
    // Get the current entity type id .For example: node, user
    $entity_type = $display_field['settings']['entity_type'];
    foreach ($entities as $entity) {
      // Get the current user.
      $user = \Drupal::currentUser();
      $token_service = \Drupal::token();
      // Replaces all tokens in a given string with appropriate values.
      $data_replace_tokens = $token_service->replace(t($data), [
        'user' => $user,
        $entity_type => $entity,
      ]);
      // Format the text perfectly with the right tag and class.
      $build["data__{$class}_{$data}"] = [
        '#markup' => "<$tag " . 'class="' . $class . '">' . "$data_replace_tokens</$tag>",
      ];
    }
    return $build;
  }

  /**
   * Gets the current view mode from the current display.
   *
   * @return string
   */
  protected function getCurrentViewMode() {
    $last_item = array_key_last(explode('/', \Drupal::request()->query->get('q')));

    return $last_item == 'display' ? 'default' : $last_item;
  }

}
