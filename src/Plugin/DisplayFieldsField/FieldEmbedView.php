<?php

namespace Drupal\display_fields\Plugin\DisplayFieldsField;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\display_fields\Element\EntityFieldSelect;
use Drupal\views\Views;

/**
 * Plugin that renders a view.
 *
 * Discover an existing field from the entity and referenced entities.
 *
 * @DisplayFieldsField(
 *   id = "embed_view",
 *   title = @Translation("Embed a view"),
 *   entity_types = {},
 * )
 */
class FieldEmbedView extends Field {

  /**
   * {@inheritdoc}
   */
  public function createForm($form, FormStateInterface $form_state, $parents = []) {
    $form['embed_view'] = [
      '#type' => 'select',
      '#title' => t('Select a view'),
      '#options' => $this->getViewsOptions(),
      '#required' => TRUE,
      '#description' => t('Choose a view to embed, you can adjust the display settings by view mode.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function createFormSubmit($form, FormStateInterface $form_state, $parents = []) {}

  /**
   * @return array
   */
  public function getViewsOptions() {
    static $views_list = [];

    if (!empty($views_list)) {
      return $views_list;
    }

    $query = \Drupal::entityQuery('view')
      ->addTag('view_access');
    $entity_ids = $query->execute();

    if (!empty($entity_ids)) {
      $entities = \Drupal::entityTypeManager()->getStorage('view')->loadMultiple($entity_ids);
      foreach ($entities as $entity) {
        $views_list[$entity->id()] = $entity->label();
      }
    }

    return $views_list;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldBuild($entities, $display_field, $display_settings, $parent_entity, $view_mode, $language) {
    foreach ($entities as $delta => $entity) {
      $args = [];
      $view = Views::getView($display_field['settings']['embed_view']);
      $view->setDisplay($display_settings['settings']['view_display'] ?? 'default');
      $view->setAjaxEnabled($display_settings['settings']['enable_ajax'] ?? TRUE);

      // Set the view URL to the current page for the exposed filter form
      // redirection.
      $view->override_url = Url::fromRoute('<current>');

      // Build the view arguments.
      if (!empty($display_settings['settings']['arguments'])) {
        $view_display = $view->getDisplay();
        foreach ($view_display['display_options']['arguments'] as $argument) {
          if (isset($display_settings['settings']['arguments'][$argument['id']])) {
            $arg_values = $this->getViewsArgumentsValues($entities, $display_field, $display_settings['settings']['arguments'][$argument['id']], $parent_entity, $view_mode, $language);
            if (!empty($arg_values)) {
              $args[] = implode('+', $arg_values);
            }
          }
        }
      }

      $view->preExecute($args);
      // Execute the view.
      $render = $view->display_handler->execute();

      $view->postExecute();

      $build[$delta] = $render;
      $build[$delta]['#weight'] = $delta;
    }

    // As the view can be dynamic with multiple pages, we need to enable a
    // different caching for this part of the entity render array, which is now
    // possible, with Drupal 8.
    $build['#cache'] = [
      'keys' => ['entity_view', $parent_entity->getEntityTypeId(), $parent_entity->id(), $display_field['field_name']],
      'contexts' => ['languages', 'url.query_args.pagers'],
      'tags' => [$parent_entity->getEntityTypeId() . ':' . $parent_entity->id() . ':' . $display_field['field_name']],
      // Let the entity top render max age keep the priority.
      // 'max-age' => 0,
    ];

    return $build;
  }

  protected function getViewsArgumentsValues($entities, $display_field, $display_settings, $parent_entity, $view_mode, $language) {
    $args = [];
    $field_name = $display_settings['field_name'];
    $reference_key = $display_settings['reference_key'] ?? $field_name;

    $reference_keys = explode(':', $reference_key);
    $current_key = reset($reference_keys);

    if (count($reference_keys) == 1) {
      // Render the fields.
      foreach ($entities as $delta => $entity) {
        if ($entity->hasField($current_key)) {
          // @todo What should be done?
          foreach ($entity->get($current_key) as $key => $value) {
            foreach ($value->getValue() as $key_value => $value_value) {
              $args[] = $value_value;
            }
          }
        }
      }

      return $args;
    }

    unset($reference_keys[0]);
    $display_settings['reference_key'] = implode(':', $reference_keys);

    $current_keys = explode('|', $current_key);

    foreach ($entities as $delta => $entity) {
      if ($entity->hasField($current_keys[0])) {
        $loaded_entities = $entity->get($current_keys[0])->referencedEntities();
        // Filter by bundle if specified.
        if ($current_keys[2] != '*') {
          foreach ($loaded_entities as $delta => $loaded_entity) {
            if ($loaded_entity->bundle() != $current_keys[2]) {
              unset($loaded_entities[$delta]);
            }
          }
        }
        // Get only the first entity referenced if specified.
        if (isset($current_keys[3]) && $current_keys[3] == 0) {
          $loaded_entities = [reset($loaded_entities)];
        }
        $args = array_merge($args, $this->getViewsArgumentsValues($loaded_entities, $display_field, $display_settings, $parent_entity, $view_mode, $language));
      }
    }

    return $args;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldFormRow($field_name, $field, $field_display_settings, $view_mode, FormStateInterface $form_state, &$complete_form) {
    $row = parent::buildFieldFormRow($field_name, $field, $field_display_settings, $view_mode, $form_state, $complete_form);

    $display_field_config = $field['settings'];

    // Check the currently selected plugin, and merge persisted values for its
    // settings.
    if ($display_type = $form_state->getValue(['fields', "display_fields_$field_name", 'type'])) {
      $field_display_settings['type'] = $display_type;
    }
    $plugin_settings = $form_state->get('plugin_settings');
    if (isset($plugin_settings["display_fields_$field_name"]['settings'])) {
      $field_display_settings['settings'] = $plugin_settings["display_fields_$field_name"]['settings'];
    }

    $row['human_name']['#markup'] .= '<br><div class="display-field-description"><small>' . t('View: @view', ['@view' => $field['settings']['embed_view']]) . '</small></div>';

    $row['#row_type'] = 'field';
    $row['#js_settings'] = [
      'rowHandler' => 'field',
      'defaultPlugin' => 'field_embed_view_formatter',
    ];

    $row['plugin'] = [
      'type' => [
        '#type' => 'select',
        '#title' => t('Plugin for @title', ['@title' => $field['label']]),
        '#title_display' => 'invisible',
        '#options' => $this->getExtraFieldVisibilityOptions(),
        '#default_value' => $field_display_settings['type'] ?? 'hidden',
        '#parents' => ['fields', "display_fields_$field_name", 'type'],
        '#attributes' => ['class' => ['field-plugin-type']],
      ],
    ];

    // Base button element for the various plugin settings actions.
    $base_button = [
      '#submit' => [[$this, 'multistepSubmit']],
      '#ajax' => [
        'callback' => [$this, 'multistepAjax'],
        'wrapper' => 'field-display-overview-wrapper',
        'effect' => 'fade',
      ],
      '#field_name' => "display_fields_$field_name",
    ];

    // Process values from formatters settings.
    // (These are set via AJAX, so check them.)
    if (!empty($field_display_settings) && $form_state->get('plugin_settings_edit') == "display_fields_$field_name") {
      // We are currently editing this field's plugin settings. Display the
      // settings form and submit buttons.
      $row['plugin']['settings_edit_form'] = [];

      // Generate the settings form and allow other modules to alter it.
      $settings_form = $this->fieldRowSettingsForm($complete_form, $form_state, $field, $field_display_settings);

      if ($settings_form) {
        $row['plugin']['#cell_attributes'] = ['colspan' => 3];
        $row['plugin']['settings_edit_form'] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['field-plugin-settings-edit-form']],
          '#parents' => ['fields', "display_fields_$field_name", 'settings_edit_form'],
          'label' => ['#markup' => t('Plugin settings')],
          'settings' => $settings_form,
          // 'third_party_settings' => $third_party_settings_form,
          'actions' => [
            '#type' => 'actions',
            'save_settings' => $base_button + [
              '#type' => 'submit',
              '#button_type' => 'primary',
              '#name' => "display_fields_{$field_name}_plugin_settings_update",
              '#value' => t('Update'),
              '#op' => 'update',
            ],
            'cancel_settings' => $base_button + [
              '#type' => 'submit',
              '#name' => "display_fields_{$field_name}_plugin_settings_cancel",
              '#value' => t('Cancel'),
              '#op' => 'cancel',
              // Do not check errors for the 'Cancel' button, but make sure we
              // get the value of the 'plugin type' select.
              '#limit_validation_errors' => [['fields', "display_fields_$field_name", 'type']],
            ],
          ],
        ];
        $row['#attributes']['class'][] = 'field-plugin-settings-editing';
      }

    }
    elseif (!empty($field_display_settings)) {
      // Display a summary of the current plugin settings, and (if the
      // summary is not empty) a button to edit them.
      $summary = $this->fieldRowSettingsSummary($complete_form, $form_state, $field, $field_display_settings);

      if (!empty($summary)) {
        $row['settings_summary'] += [
          '#type' => 'markup',
          '#markup' => '<div class="field-plugin-summary">' . $summary . '</div>',
          '#cell_attributes' => ['class' => ['field-plugin-summary-cell']],
        ];
      }

      // Check selected plugin settings to display edit link or not.
      $settings_form = $this->fieldRowSettingsForm($complete_form, $form_state, $field, $field_display_settings);
      if (!empty($settings_form)) {
        $row['settings_edit'] += $base_button + [
          '#type' => 'image_button',
          '#name' => "display_fields_{$field_name}_settings_edit",
          '#src' => 'core/misc/icons/787878/cog.svg',
          '#attributes' => ['class' => ['field-plugin-settings-edit'], 'alt' => t('Edit')],
          '#op' => 'edit',
          // Do not check errors for the 'Edit' button, but make sure we get
          // the value of the 'plugin type' select.
          '#limit_validation_errors' => [['fields', "display_fields_$field_name", 'type']],
          '#prefix' => '<div class="field-plugin-settings-edit-wrapper">',
          '#suffix' => '</div>',
        ];
      }
    }
    return $row;
  }

  /**
   * Form submission handler for multistep buttons.
   */
  public function multistepSubmit($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $op = $trigger['#op'];

    switch ($op) {
      case 'edit':
        // Store the field whose settings are currently being edited.
        $field_name = $trigger['#field_name'];
        $form_state->set('plugin_settings_edit', $field_name);
        break;

      case 'update':
        // Store the saved settings, and set the field back to 'non edit' mode.
        $field_name = $trigger['#field_name'];
        if ($plugin_settings = $form_state->getValue(['fields', $field_name, 'settings_edit_form', 'settings'])) {
          $form_state->set(['plugin_settings', $field_name, 'settings'], $plugin_settings);
        }

        $form_state->set('plugin_settings_edit', NULL);
        break;

      case 'cancel':
        // Set the field back to 'non edit' mode.
        $form_state->set('plugin_settings_edit', NULL);
        break;

      case 'refresh_table':
        // If the currently edited field is one of the rows to be refreshed, set
        // it back to 'non edit' mode.
        $updated_rows = explode(' ', $form_state->getValue('refresh_rows'));
        $plugin_settings_edit = $form_state->get('plugin_settings_edit');
        if ($plugin_settings_edit && in_array($plugin_settings_edit, $updated_rows)) {
          $form_state->set('plugin_settings_edit', NULL);
        }
        break;
    }

    $form_state->setRebuild();
  }

  /**
   * Ajax handler for multistep buttons.
   */
  public function multistepAjax($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $op = $trigger['#op'];

    $updated_rows = [];
    $updated_columns = [];
    // Pick the elements that need to receive the ajax-new-content effect.
    switch ($op) {
      case 'edit':
        $updated_rows = [$trigger['#field_name']];
        $updated_columns = ['plugin'];
        break;

      case 'update':
      case 'cancel':
        $updated_rows = [$trigger['#field_name']];
        $updated_columns = ['plugin', 'settings_summary', 'settings_edit'];
        break;

      case 'refresh_table':
        $updated_rows = array_values(explode(' ', $form_state->getValue('refresh_rows')));
        $updated_columns = ['settings_summary', 'settings_edit'];
        break;

    }

    foreach ($updated_rows as $name) {
      foreach ($updated_columns as $key) {
        $element = &$form['fields'][$name][$key];
        $element['#prefix'] = '<div class="ajax-new-content">' . ($element['#prefix'] ?? '');
        $element['#suffix'] = ($element['#suffix'] ?? '') . '</div>';
      }
    }

    // Return the whole table.
    return $form['fields'];
  }

  /**
   * @param array $form
   * @param Drupal\Core\Form\FormStateInterface $form_state
   * @param $field
   * @param $field_display_settings
   */
  public function fieldRowSettingsForm($form, FormStateInterface $form_state, $field, $field_display_settings) {
    $display_settings = $field_display_settings['settings'] ?? [];
    $embed_view = $field['settings']['embed_view'];
    $view = \Drupal::service('entity_type.manager')->getStorage('view')->load($embed_view);
    $display_options = [];

    $default_display_id = $display_settings['view_display'] ?? 'default';

    foreach ($view->get('display') as $display) {
      $display_options[$display['id']] = $display['display_title'];
    }

    $elements['view_display'] = [
      '#type' => 'select',
      '#title' => t('Display'),
      '#options' => $display_options,
      '#default_value' => $default_display_id,
      '#description' => t('The view display used for render.'),
      '#weight' => 0,
    ];

    $elements['enable_ajax'] = [
      '#type' => 'checkbox',
      '#title' => t('Force ajax enabled'),
      '#default_value' => $display_settings['enable_ajax'] ?? FALSE,
      '#weight' => 0,
    ];
    $display = $view->getDisplay($default_display_id);

    $build['fields'] = [];
    $executable = $view->getExecutable();
    $executable->setDisplay($default_display_id);

    static $relationships = NULL;
    if (!isset($relationships)) {
      // Get relationship labels.
      $relationships = [];
      foreach ($executable->display_handler->getHandlers('relationship') as $id => $handler) {
        $relationships[$id] = $handler->adminLabel();
      }
    }

    $elements['arguments']['#tree'] = TRUE;
    $elements['arguments']['#prefix'] = '<h4>' . t('Arguments:') . '</h4>';

    if (!isset($display['display_options']['arguments'])) {
      // No argument found.
      $elements['arguments']['message'] = [
        '#markup' => t('No argument found on this display.'),
      ];
    }
    else {
      foreach ($display['display_options']['arguments'] as $argument) {
        $handler = $executable->display_handler->getHandler('argument', $argument['id']);
        $field_name = $handler->adminLabel(TRUE);
        if (!empty($argument['relationship']) && !empty($relationships[$argument['relationship']])) {
          $field_name = '(' . $relationships[$argument['relationship']] . ') ' . $field_name;
        }
        $elements['arguments'][$argument['id']] = [
          '#title' => $field_name,
          '#type' => 'entity_field_select',
          '#entity_type' => $this->getEntityTypeId(),
          '#bundle' => $this->bundle(),
          '#required' => FALSE,
          '#default_value' => $display_settings['arguments'][$argument['id']] ?? NULL,
          '#description' => t('You can set the arguments value from a field of this entity and those possible referenced entities.'),
        ];
      }
    }
    return $elements;
  }

  /**
   * @param array $form
   * @param Drupal\Core\Form\FormStateInterface $form_state
   * @param $field
   * @param $field_display_settings
   */
  public function fieldRowSettingsSummary($form, FormStateInterface $form_state, $field, $field_display_settings) {
    $embed_view = $field['settings']['embed_view'];
    $view = \Drupal::service('entity_type.manager')->getStorage('view')->load($embed_view);
    $field_display_settings['settings']['view_display'] = $field_display_settings['settings']['view_display'] ?? 'default';

    $display = $view->getDisplay($field_display_settings['settings']['view_display']);

    $executable = $view->getExecutable();
    $executable->setDisplay($field_display_settings['settings']['view_display']);

    $arguments = '';
    if (!empty($field_display_settings['settings']['arguments'])) {
      static $relationships = NULL;
      if (!isset($relationships)) {
        // Get relationship labels.
        $relationships = [];
        foreach ($executable->display_handler->getHandlers('relationship') as $id => $handler) {
          $relationships[$id] = $handler->adminLabel();
        }
      }

      foreach ($field_display_settings['settings']['arguments'] as $id => $field_display_settings_argument) {
        $handler = $executable->display_handler->getHandler('argument', $id);
        if ($handler) {
          $field_name = $handler->adminLabel(TRUE);
          if (!empty($display['display_options']['arguments'][$id]['relationship']) && !empty($relationships[$display['display_options']['arguments'][$id]['relationship']])) {
            $field_name = '(' . $relationships[$display['display_options']['arguments'][$id]['relationship']] . ') ' . $field_name;
          }

          $arguments .= "<li>$field_name: <br />" . EntityFieldSelect::getBreadcrumbReferences($field_display_settings_argument) . '</li>';
        }
      }
    }
    else {
      $arguments = '<li>' . t('No argument found on this display.') . '</li>';
    }

    $force_ajax = !empty($field_display_settings['settings']['enable_ajax']) ? t('Yes') : t('No');
    return implode('<br />', [
       t('Display: @view_display', ['@view_display' => $field_display_settings['settings']['view_display']]),
       t('Force AJAX: @ajax', ['@ajax' => $force_ajax]),
       t('Arguments: <ul>@arguments</ul>', ['@arguments' => $arguments]),
    ]);
  }

}
